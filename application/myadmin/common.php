<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Session;
// 应用公共文件
	// POSTHTTP请求
	function GeturlGet($url)
	{
		$ch=curl_init($url);
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,false);
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);
		return curl_exec($ch);
	}
	// GET请求
	function GeturlPost($url, $data = null)
	{
	    $curl = curl_init();
	    curl_setopt($curl, CURLOPT_URL, $url);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
	    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
	    if (!empty($data)){
	        curl_setopt($curl, CURLOPT_POST, 1);
	        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	    }
	    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	    $output = curl_exec($curl);
	    curl_close($curl);
	    return $output;
	}
	// md5密码
	function Getmd5Pass($str)
	{
		$salt = "guoguo";	// 加盐
		return md5(md5($str).$salt);
	}
	// 获取IP
	function GetIp()
	{
	    if (getenv("HTTP_CLIENT_IP")) {
	        $ip = getenv("HTTP_CLIENT_IP");
	    } else if (getenv("HTTP_X_FORWARDED_FOR")) {
	        $ip = getenv("HTTP_X_FORWARDED_FOR");
	    } else if (getenv("REMOTE_ADDR")) {
	        $ip = getenv("REMOTE_ADDR");
	    } else {
	        $ip = $_SERVER["REMOTE_ADDR"];
	    }
	    return $ip;
	}
	// 去除前后空格
	function TrimArray($data=[])
	{
		if(count($data)>0){
			foreach($data as $k=>$v){
				$data[$k]=trim($v);
			}
		}
		return $data;
	}
	// 清除session和cache
	function OutClear()
	{
		Session::delete("authList_".Session::get("adminId"));
		Session::delete("adminId");
		Session::delete("userName");
		Session::delete("cateId");
		Session::delete("nickName");
		return true;
	}
	// 删除文件
	function DelClear($dir)
	{
		if(!is_dir($dir)){
			return false;
		}
		$dh=opendir($dir);
		while ($file=readdir($dh)) {
			if($file!="." && $file!="..") {
				$fullpath=$dir."/".$file;
				if(!is_dir($fullpath)) {
					@unlink($fullpath);
				} else {
					DelClear($fullpath);
				}
			}
		}
		closedir($dh);
		// 删除当前文件夹：
		if(rmdir($dir)) {
	     return true;
		} else {
			return false;
		}
	}
	// 权限判断
	function MyAuto($module,$controller,$action)
	{
		$CacheData=Session::get("authList_".Session::get("adminId"));
		$curl=strtolower($module."/".$controller."/".$action);
		if($curl=="myadmin/index/index"){
			return true;
		}
		if($curl=="myadmin/index/welcome"){
			return true;
		}
		if($curl=="myadmin/index/clear"){
			return true;
		}
		if($curl=="myadmin/admin/myinfo"){
			return true;
		}
		if($curl=="myadmin/article/upload"){
			return true;
		}
		if($curl=="myadmin/order/show"){
			return true;
		}
		if($CacheData){
			if (in_array($curl,$CacheData)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	// 删除数据
	function DelTable($Int,$Tbalename)
	{
		if($Tbalename=="config" || $Tbalename=="menu"){
			$Int=SubsetTable($Int,$Tbalename);
		}
		//lock是1的话，系统参数暂不能删除
		$info=Db::name($Tbalename)->where("lock",2)->delete(explode(",",$Int));
		if($info){
			return ["code"=>0,"msg"=>"操作成功"];
		}else{
			return ["code"=>1,"msg"=>"操作失败"];
		}
	}
	// 获取子集
	function SubsetTable($Int,$Tbalename)
	{
		$Ids=Db::name($Tbalename)->where("pid",$Int)->column("id");
		if(count($Ids)!=0){
			foreach($Ids as $k=>$v){
				$cIds=Db::name($Tbalename)->where("pid",$v)->column("id");
				if(count($cIds)>0){
					array_push($Ids,implode(",",$cIds));
				}
			}
			array_push($Ids,$Int);
			return implode(",",$Ids);
		}else{
			return $Int;
		}
	}
	// 改变数据状态、排序
	function ChangeTable($Data,$Tbalename)
	{
		if($Tbalename==""){
			return ["code"=>1,"msg"=>"参数错误"];
		}else{
			$Data["update_time"]=time();
			$info=Db::name($Tbalename)->update($Data);
			if($info){
				return ["code"=>0,"msg"=>"操作成功"];
			}else{
				return ["code"=>1,"msg"=>"操作失败"];
			}
		}
	}
	// 上传图片
	function UploadThumb($File,$Tbalename="")
	{
		$FileSize = 1024*1024;	// 设定文件大小1M
		$Filetype = ["jpg", "jpeg", "png", "bmp"];	// 设定文件类型
		$info = $File->validate(["size"=>$FileSize,"ext"=>$Filetype])->move("uploads");
		if($info){
			// 插入数据库
			$data["Ip"] = GetIp();
			$data["adminId"] = Session::get("adminId");
			$data["name"] = $info->getFilename();
			$data["topic"] = "uploads/".date("Ymd")."/".$info->getFilename();
			$data["size"] = round($info->getSize()/1024,2);
			$data["ext"] = $info->getExtension();
			$data["create_time"] = time();
			$pid=Db::name("files")->insertGetId($data);
			if($pid){
				// 返回缩略图路径
				return ["code"=>0,"msg"=>"操作成功","data"=>["name"=> $data["name"],"size"=>$data["size"],"topic"=>$data["topic"],"pid"=>$pid]];
			}else{
				return ["code"=>1,"msg"=>"参数错误"];
			}
		}else{
			return ["code"=>1,"msg"=>$File->getError()];
		}
	}