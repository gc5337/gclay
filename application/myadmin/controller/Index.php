<?php
namespace app\myadmin\controller;
use think\facade\Session;
use think\Db;
class Index extends Base
{
    public function index()
    {
		$cate=Db::name("cate")->where("id",Session::get("cateId"))->value("permissions");
		$where["id"] = explode(",",$cate);
		$where["type"]=1;
		$where["status"]=1;
		$menuData=Model("Menu")->index($where);
		return view("", [
			"menuData"=>$menuData,
			"webTitle"  => Db::name("config")->where("name","title")->value("value")?:"",
			"adminName"  	=> Session::get("nickName")?:"",
		]);
    }
	public function welcome()
	{
		return view();
	}
	// 清除
	public function clear()
	{
		$path=\think\facade\Env::get("runtime_path");
		$res=DelClear($path);
		if($res){
			return ["code"=>0,"msg"=>"清理成功"];
		}else{
			return ["code"=>1,"msg"=>"清理失败"];
		}
	}
}
