<?php
namespace app\myadmin\controller;
use think\Db;
class Cate extends Base
{
	// 列表
    public function index()
    {
    	if($this->request->isPost()) {
    	    $post = $this->request->post();
    		$res=Model("Cate")->index($post);
    		return $res;
    	} else {
    	    return view();
    	}
    }
	// 增改
	public function edit()
	{
		$id = $this->request->has("id") ? $this->request->param("id", 0, "intval") : 0;
		$info=Db::name("cate")->where("id",$id)->find();
		if($this->request->isPost()) {
		    $post = $this->request->post();
			$res=Model("Cate")->edit($id,$post);
			return $res;
		} else {
			return view("", [
				"info"		=>$info
			]);
		}
	}
	// 删除
	public function del()
	{
		if($this->request->isPost()) {
		    $post = $this->request->post("id");
			$res=DelTable($post,"cate");
			return $res;
		}
	}
	// 状态
	public function status()
	{
		if($this->request->isPost()) {
		   $post = $this->request->post();
		   $res=ChangeTable($post,"cate");
		   return $res;
		}
	}
}
