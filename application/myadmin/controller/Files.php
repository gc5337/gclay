<?php
namespace app\myadmin\controller;
use think\Db;
class Files extends Base
{
	// 列表
    public function index()
    {
    	if($this->request->isPost()) {
    	    $post = $this->request->post();
    		$res=Model("Files")->index($post);
    		return $res;
    	} else {
    	    return view();
    	}
    }
	// 删除
	public function del()
	{
		if($this->request->isPost()) {
		    $post = $this->request->post("id");
			$res=Model("Files")->del($post);
			return $res;
		}
	}
}
