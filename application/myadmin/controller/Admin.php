<?php
namespace app\myadmin\controller;
use think\facade\Session;
use think\Db;
class Admin extends Base
{
	// 列表
    public function index()
    {
		if($this->request->isPost()) {
		    $post = $this->request->post();
			$res=Model("Admin")->index($post);
			return $res;
		} else {
		    return view();
		}
	}
	// 增改
	public function edit()
	{
		$id = $this->request->has("id") ? $this->request->param("id", 0, "intval") : 0;
		$info=Db::name("admin")->where("id",$id)->find();
		$cateData=Db::name("cate")->select();
		if($this->request->isPost()) {
		    $post = $this->request->post();
			$res=Model("Admin")->edit($id,$post);
			return $res;
		} else {
			return view("", [
				"info"=>$info,
				"cateData"=>$cateData
			]);
		}
	}
	// 个人
	public function myinfo()
	{
		if($this->request->isPost()) {
		    $post = $this->request->post();
			$res=Model("Admin")->myedit($post);
			return $res;
		} else {
		    return view("", [
		    	"info"=>Db::name("admin")->where("id",Session::get("adminId"))->find()
		    ]);
		}
	}
	// 删除
	public function del()
	{
		if($this->request->isPost()) {
		    $post = $this->request->post("id");
			$res=DelTable($post,"admin");
			return $res;
		}
	}
	// 状态
	public function status()
	{
		if($this->request->isPost()) {
		   $post = $this->request->post();
		   $res=ChangeTable($post,"admin");
		   return $res;
		}
	}
}
