<?php
namespace app\myadmin\controller;
use think\Db;
class Config extends Base
{
    // 列表
    public function index()
    {
		if($this->request->isPost()) {
			$post = $this->request->post();
			$info=Model("Config")->index();
			return ["code"=>0,"msg"=>"获取成功","data"=>$info];
		}else{
			return view();
		}
    }
	// 增改
	public function edit()
	{
		$id = $this->request->has("id") ? $this->request->param("id", 0, "intval") : 0;
		$pData=Db::name("config")->where("pid",0)->order("orders asc,id asc")->field("id,name_cn")->select();
		$info=Db::name("config")->where("id",$id)->find();
		if($this->request->isPost()) {
			$post = $this->request->post();
			$res=Model("Config")->edit($id,$post);
			return $res;
		}else{
			return view("", [
				"info"=>$info,
				"pData"=>$pData,
			]);
		}
	}
	// 删除
	public function del()
	{
		if($this->request->isPost()) {
		    $post = $this->request->post("id");
		    $res=DelTable($post,"config");
		    return $res;
		}
	}
	// 状态
	public function status()
	{
		if($this->request->isPost()) {
		    $post = $this->request->post();
		    $res=ChangeTable($post,"config");
		    return $res;
		}
	}
	// 排序
	public function orders()
	{
		if($this->request->isPost()) {
		    $post = $this->request->post();
			$post["orders"] = intval($post["orders"])?:0;
			$res=ChangeTable($post,"config");
			return $res;
		}
	}
	// 配置
	public function myconfig()
	{
		$where["status"]=1;
		$res=Model("Config")->index($where);
		return view("", [
			"info"=>$res
		]);
	}
	// 编辑
	public function myedit()
	{
		if($this->request->isPost()) {
			$post = $this->request->post();
			$res=Model("Config")->myedit($post);
			return $res;
		}
	}
}
