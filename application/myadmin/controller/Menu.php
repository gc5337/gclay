<?php
namespace app\myadmin\controller;
use think\Db;
class Menu extends Base
{
    // 列表
    public function index()
    {
		if($this->request->isPost()) {
			$post = $this->request->post();
			$menuData=Model("Menu")->index();
			return ["code"=>0,"msg"=>"获取成功","data"=>$menuData];
		}else{
			return view();
		}
    }
	// 增改
	public function edit()
	{
		$id = $this->request->has("id") ? $this->request->param("id", 0, "intval") : 0;
		$info=Db::name("menu")->where("id",$id)->find();
		$menuData=Model("Menu")->index();
		if($this->request->isPost()) {
		    $post = $this->request->post();
			$res=Model("Menu")->edit($id,$post);
			return $res;
		} else {
			return view("", [
				"info"=>$info,
				"menuData"=>$menuData
			]);
		}
	}
	// 删除
	public function del()
	{
		if($this->request->isPost()) {
			$post = $this->request->post("id");
			$res=DelTable($post,"menu");
			return $res;
		}
	}
	// 状态
	public function status()
	{
		if($this->request->isPost()) {
		    $post = $this->request->post();
			$res=ChangeTable($post,"menu");
			return $res;
		}
	}
	// 排序
	public function orders()
	{
		if($this->request->isPost()) {
		    $post = $this->request->post();
			$post["orders"] = intval($post["orders"])?:0;
			$res=ChangeTable($post,"menu");
			return $res;
		}
	}
}
