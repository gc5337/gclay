<?php
namespace app\myadmin\controller;
use think\Controller;
use think\facade\Session;
use think\facade\Cookie;
use think\Db;
class Login extends Controller
{
	protected $middleware = [
	    "check"=>["only"=>["index"]],	// 仅index执行
	];
	// 登录
    public function index()
    {
		if($this->request->isPost()) {
			$post = $this->request->post();
			$res=Model("Login")->index($post);
			return $res;
		}else{
			return view("", [
				"webTitle"  => Db::name("config")->where("name","title")->value("value")?:"",
				"userName"  => Cookie::get("userName")?:""
			]);
		}
	}
	// 退出
	public function loginout()
	{
		if ($this->request->isAjax()) {
			OutClear();
			if(!Session::has("adminId") || !Session::has("userName") || Session::has("cateId")) {
				return ["code"=>0,"msg"=>"正在退出"];
			} else {
				return ["code"=>1,"msg"=>"操作失败"];
			}
		}else{
			return ["code"=>1,"msg"=>"参数错误"];
		}
	}
	// 判断
	public function login_is_none()
	{
		if ($this->request->isAjax()) {
			if(!Session::has("adminId") || !Session::has("cateId")){
				OutClear();
				return ["code"=>0,"msg"=>"登录超时，请登录..."];
			}else{
				return ["code"=>1];
			}
		}else{
			return ["code"=>1,"msg"=>"参数错误"];
		}
	}
}
