<?php
namespace app\myadmin\model;
use think\Model;
use think\facade\Session;
class Admin extends Model
{
	// 时间戳配置
	protected $autoWriteTimestamp = true;
	// 定义时间戳字段名
	protected $createTime = "create_time";
	protected $updateTime = "update_time";
	// 列表
	public function index($Data)
	{
		$cur_page = intval($Data["page"])-1;	//默认前端page传过来为1
		$count=$this->count();
		$info=$this->limit(($cur_page*$Data["limit"]),$Data["limit"])->order("id asc")
			->alias("a")
			->leftJoin("cate b","b.id = a.cateId")
			->field("a.*,b.name as catename")
			->select();
		if($info){
			return ["code"=>0,"msg"=>"获取成功","data"=>$info,"count"=>$count];
		}else{
			return ["code"=>1,"msg"=>"暂无内容"];
		}
	}
	// 增改
	public function edit($Int,$Data)
	{
		$Data=TrimArray($Data);
		$validate = new \app\myadmin\validate\Admin;
		if (!$validate->check($Data)) {
			return ["code"=>1,"msg"=>$validate->getError()];
		}
		$Data["password"]=Getmd5Pass($Data["password_old"]);
		if($Int==0){
			$info=$this->save($Data);
		}else{
			$info=$this->update($Data);
		}
		if($info){
			return ["code"=>0,"msg"=>"操作成功"];
		}else{
			return ["code"=>1,"msg"=>"操作失败"];
		}
	}
	// 编辑
	public function myedit($Data)
	{
		$password_old=$this->where("id",$Data["id"])->value("password_old");
		$Data=TrimArray($Data);
		if($Data["nickname"]==""){
			return ["code"=>1,"msg"=>"昵称为空"];
		}
		if($Data["password_old"]==""){
			return ["code"=>1,"msg"=>"密码为空"];
		}
		$Data["password"]=Getmd5Pass($Data["password_old"]);
		$info=$this->update($Data);
		if($info){
			if(Session::get("nickName")!=$Data["nickname"]){
				Session::set("nickName",$Data["nickname"]);
			}
			if($password_old!=$Data["password_old"]){
				OutClear();
				return ["code"=>2,"msg"=>"操作成功，跳转中..."];
			}
			return ["code"=>0,"msg"=>"操作成功"];
		}else{
			return ["code"=>1,"msg"=>"操作失败"];
		}
	}
}
