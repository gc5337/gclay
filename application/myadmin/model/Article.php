<?php
namespace app\myadmin\model;
use think\Model;
use think\facade\Session;
use think\db\Where;
class Article extends Model
{
	// 时间戳配置
	protected $autoWriteTimestamp = true;
	// 定义时间戳字段名
	protected $createTime = "create_time";
	protected $updateTime = "update_time";
	// 列表
	public function index($Data)
	{
		$Data=TrimArray($Data);
		$where = new Where;
		if(Session::get("cateId")!=1){
			$where["adminId"]=Session::get("adminId");
		}
		if(!empty($Data["title"]) && $Data["title"]!=""){
			$where["title"] = ["like", "%".$Data["title"]."%"];
		}
		if(!empty($Data["status"]) && $Data["status"]!=""){
			$where["status"]=$Data["status"];
		}
		if(!empty($Data["top"]) && $Data["top"]!=""){
			$where["top"]=$Data["top"];
		} 
		if(!empty($Data["create_time"]) && $Data["create_time"]!=""){
			$where["create_time"] = [["egt",strtotime($Data["create_time"])],["elt",strtotime($Data["create_time"])+86399]];
		}
		
		$cur_page = intval($Data["page"])-1;	//默认前端page传过来为1
		$count=$this->where($where)->count();
		$info=$this->where($where)->limit(($cur_page*$Data["limit"]),$Data["limit"])->order("id desc")->select();
		if($info){
			return ["code"=>0,"msg"=>"获取成功","data"=>$info,"count"=>$count];
		}else{
			return ["code"=>1,"msg"=>"暂无内容"];
		}
	}
	// 增改
	public function edit($Int,$Data)
	{
		$Data=TrimArray($Data);
		unset($Data["file"]);
		$validate = new \app\myadmin\validate\Article;
		if (!$validate->check($Data)) {
			return ["code"=>1,"msg"=>$validate->getError()];
		}
		if($Int==0){
			$Data["Ip"]=GetIp();
			$Data["adminId"]=Session::get("adminId");
			$info=$this->save($Data);
		}else{
			$info=$this->update($Data);
		}
		if($info){
			return ["code"=>0,"msg"=>"操作成功"];
		}else{
			return ["code"=>1,"msg"=>"操作失败"];
		}
	}
}
