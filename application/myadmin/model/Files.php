<?php
namespace app\myadmin\model;
use think\Model;
use think\facade\Session;
use think\db\Where;
class Files extends Model
{
	// 时间戳配置
	protected $autoWriteTimestamp = true;
	// 定义时间戳字段名
	protected $createTime = "create_time";
	protected $updateTime = "update_time";
	// 列表
	public function index($Data)
	{
		$where = new Where;
		if(Session::get("cateId")!=1){
			$where["adminId"]=Session::get("adminId");
		}
		$cur_page = intval($Data["page"])-1;	//默认前端page传过来为1
		$count=$this->where($where)->count();
		
		$info=$this->where($where)->limit(($cur_page*$Data["limit"]),$Data["limit"])->order("id desc")
			->alias("a")
			->leftJoin("admin b","b.id = a.adminId")
			->field("a.*,b.nickname")
			->select();
		if($info){
			return ["code"=>0,"msg"=>"获取成功","data"=>$info,"count"=>$count];
		}else{
			return ["code"=>1,"msg"=>"暂无内容"];
		}
	}
	// 删除
	public function del($Int)
	{
		$Int_more=explode(",",$Int);
		//删除附件
		foreach($Int_more as $k=>$v){
			$topic = $this->where("id",$v)->value("topic");
			if(file_exists($topic)) {
				@unlink($topic);
			}
		}
		//删除数据
		$info=$this->destroy($Int);
		if ($info) {
		    return ["code"=>0,"msg"=>"成功"];
		} else {
		    return ["code"=>1,"msg"=>"失败"];
		}
	}
}
