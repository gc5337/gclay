<?php
namespace app\myadmin\model;
use think\Model;
class Cate extends Model
{
	// 时间戳配置
	protected $autoWriteTimestamp = true;
	// 定义时间戳字段名
	protected $createTime = "create_time";
	protected $updateTime = "update_time";
	// 列表
	public function index($Data)
	{
		$cur_page = intval($Data["page"])-1;	//默认前端page传过来为1
		$count=$this->count();
		$info=$this->limit(($cur_page*$Data["limit"]),$Data["limit"])->order("id asc")->select();
		if($info){
			return ["code"=>0,"msg"=>"获取成功","data"=>$info,"count"=>$count];
		}else{
			return ["code"=>1,"msg"=>"暂无内容"];
		}
	}
	// 增改
	public function edit($Int,$Data)
	{
		$Data=TrimArray($Data);
		$validate = new \app\myadmin\validate\Cate;
		if (!$validate->check($Data)) {
			return ["code"=>1,"msg"=>$validate->getError()];
		}
		if($Int==0){
			$info=$this->save($Data);
		}else{
			$info=$this->update($Data);
		}
		if($info){
			return ["code"=>0,"msg"=>"操作成功"];
		}else{
			return ["code"=>1,"msg"=>"操作失败"];
		}
	}
}
