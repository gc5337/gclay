<?php
namespace app\myadmin\middleware;
use think\facade\Session;
class Check
{
    public function handle($request, \Closure $next)
    {
		// 登录
		if(Session::has("adminId") || Session::has("cateId")) {
			echo "<script>location.href='/myadmin';</script>";
			die();
		}
		return $next($request);
    }
}
