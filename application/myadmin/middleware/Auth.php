<?php
namespace app\myadmin\middleware;
use think\facade\Session;
class Auth
{
    public function handle($request, \Closure $next)
    {
		// 登录
		if(!Session::has("adminId") || !Session::has("cateId")) {
			OutClear();
			echo "<script>top.location.href='/myadmin/login/index';</script>";
			die();
		}
		// 权限
		$res=MyAuto($request->module(),$request->controller(),$request->action());
		if($res==false){
			return ["code"=>1,"msg"=>"暂无权限"];exit();
		}
		return $next($request);
    }
}
