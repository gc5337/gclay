<?php
namespace app\myadmin\validate;
use think\Validate;
class Cate extends Validate
{
	// 验证validate
	protected $rule = [
	    "name"					=>"require",
		"permissions"			=>"require",
	];
	protected $message = [
		"name.require"			=>"名称为空",
		"permissions.require"	=>"权限为空",
	];
}