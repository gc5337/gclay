<?php
namespace app\myadmin\validate;
use think\Validate;
class Config extends Validate
{
	// 验证validate
	protected $rule = [
		"pid"					=>"require",
		"name_cn"				=>"require",
		"name"					=>"require|unique:config",
	];
	protected $message = [
		"pid.require"			=>"菜单为空",
		"name_cn.require"		=>"名称为空",
		"name.require"			=>"字段为空",
		"name.unique"			=>"字段存在",
	];
}