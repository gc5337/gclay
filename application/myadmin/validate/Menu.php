<?php
namespace app\myadmin\validate;
use think\Validate;
class Menu extends Validate
{
	// 验证validate
	protected $rule = [
	    "pid"					=>"require",
	    "type"					=>"require",
		"title"					=>"require",
	];
	protected $message = [
		"pid.require"			=>"菜单为空",
		"type.require"     		=>"类型为空",
		"title.require"			=>"名称为空",
	];
}