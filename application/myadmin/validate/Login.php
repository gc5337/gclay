<?php
namespace app\myadmin\validate;
use think\Validate;
class Login extends Validate
{
	// 验证validate
	protected $rule = [
	    "username"				=>"require",
	    "password"				=>"require",
		"verify"				=>"require",
	];
	protected $message = [
		"username.require"		=>"账号为空",
		"password.require"     	=>"密码为空",
		"verify.require"   		=>"滑动验证",
	];
}