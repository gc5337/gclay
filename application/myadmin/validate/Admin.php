<?php
namespace app\myadmin\validate;
use think\Validate;
class Admin extends Validate
{
	// 验证validate
	protected $rule = [
	    "cateId"				=>"require",
	    "username"				=>"require|unique:admin",
		"nickname"				=>"require",
		"password_old"			=>"require",
		"status"				=>"require",
	];
	protected $message = [
		"cateId.require"		=>"角色为空",
		"username.require"     	=>"账号为空",
		"username.unique"		=>"账号存在",
		"nickname.require"     	=>"昵称为空",
		"password_old.require"  =>"密码为空",
		"status.require"   		=>"状态为空",
	];
}