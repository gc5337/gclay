<?php
namespace app\myadmin\validate;
use think\Validate;
class Article extends Validate
{
	// 验证validate
	protected $rule = [
	    "title"					=>"require",
	];
	protected $message = [
		"title.require"			=>"标题为空",
	];
}