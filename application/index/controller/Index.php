<?php
namespace app\index\controller;
use think\Controller;
class Index extends Controller
{
    public function index()
    {
        //重定向到指定的URL地址 并且使用302
        $this->redirect('/myadmin',302);
    }
}
