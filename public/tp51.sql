/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : tp51

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2023-08-18 16:16:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gc_admin
-- ----------------------------
DROP TABLE IF EXISTS `gc_admin`;
CREATE TABLE `gc_admin` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `username` varchar(50) DEFAULT NULL COMMENT '账号',
  `password` varchar(50) DEFAULT NULL COMMENT '密码',
  `password_old` varchar(50) DEFAULT NULL COMMENT '未加密的密码',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `login_time` int(11) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `loginIp` varchar(100) DEFAULT NULL COMMENT '最后登录ip',
  `cateId` int(5) NOT NULL DEFAULT '0' COMMENT '管理员分组',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1启用，2禁用',
  `postscript` varchar(255) DEFAULT NULL COMMENT '备注',
  `lock` int(1) NOT NULL DEFAULT '2' COMMENT '参数：1锁定，2开放',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员表';

-- ----------------------------
-- Records of gc_admin
-- ----------------------------
INSERT INTO `gc_admin` VALUES ('1', '走在刀口的人', 'guoguo', '272fd78f63af3e2cd9f168cfa8c3f741', '123456', '1685597143', '1686023984', '1692346050', '127.0.0.1', '1', '1', null, '1');
INSERT INTO `gc_admin` VALUES ('2', '武功人民', 'demo', '272fd78f63af3e2cd9f168cfa8c3f741', '123456', '1685597143', '1687788098', '1688711485', '127.0.0.1', '2', '1', '', '2');

-- ----------------------------
-- Table structure for gc_article
-- ----------------------------
DROP TABLE IF EXISTS `gc_article`;
CREATE TABLE `gc_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `title` varchar(50) DEFAULT NULL COMMENT '名称',
  `image` varchar(50) DEFAULT NULL COMMENT '图片',
  `content` text COMMENT '内容',
  `postscript` varchar(255) DEFAULT NULL COMMENT '备注',
  `lock` int(1) NOT NULL DEFAULT '2' COMMENT '参数：1锁定，2开放',
  `top` int(1) NOT NULL DEFAULT '2' COMMENT '置顶1是，2否',
  `Ip` varchar(50) DEFAULT NULL COMMENT '创建Ip',
  `adminId` int(10) NOT NULL DEFAULT '0' COMMENT '创建人',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1 启用，2禁用',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='文章表';

-- ----------------------------
-- Records of gc_article
-- ----------------------------
INSERT INTO `gc_article` VALUES ('1', 'Vue—易学易用，性能出色，适用场景丰富的 Web 前端框架。', '', '<p style=\"text-align:center;\"><img src=\"/uploads/20230818/481a55642ab508e5c8d881c57798e482.jpg\" style=\"max-width:100%;\" width=\"100%\" contenteditable=\"false\"/><br/></p>', '', '2', '1', '127.0.0.1', '1', '1', '1686315426', '1692345475');
INSERT INTO `gc_article` VALUES ('2', '测试', '', '', '', '2', '2', '127.0.0.1', '1', '1', '1687703162', '1688118341');
INSERT INTO `gc_article` VALUES ('3', '测试2', '26', '<p>测试2</p>', '', '2', '1', '127.0.0.1', '2', '1', '1688711599', '1691146949');

-- ----------------------------
-- Table structure for gc_cate
-- ----------------------------
DROP TABLE IF EXISTS `gc_cate`;
CREATE TABLE `gc_cate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `permissions` varchar(200) DEFAULT NULL COMMENT '权限菜单',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1 启用，2禁用',
  `postscript` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `lock` int(1) NOT NULL DEFAULT '2' COMMENT '参数：1锁定，2开放',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='权限表';

-- ----------------------------
-- Records of gc_cate
-- ----------------------------
INSERT INTO `gc_cate` VALUES ('1', '超级管理', '2,4,13,14,15,16,17,5,18,19,20,21,22,9,23,24,25,26,10,27,28,29,30,3,11,12,31,32,33,34,35,36,37,38,39', '1', '全部权限。', '1685597143', '1692346539', '1');
INSERT INTO `gc_cate` VALUES ('2', '文章管理', '31,32,33,34,35,36,37,38,39', '1', '', '1685597143', '1692346544', '2');

-- ----------------------------
-- Table structure for gc_config
-- ----------------------------
DROP TABLE IF EXISTS `gc_config`;
CREATE TABLE `gc_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` int(10) NOT NULL DEFAULT '0' COMMENT '分类，0是顶级',
  `name` varchar(50) DEFAULT NULL COMMENT '字段名',
  `name_cn` varchar(50) DEFAULT NULL COMMENT '中文名称',
  `value` text COMMENT '字段参数',
  `type` int(1) unsigned zerofill NOT NULL DEFAULT '0' COMMENT '1输入框，2文本域，3单选框，4复选框，5下拉框',
  `orders` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `content` varchar(255) DEFAULT NULL COMMENT '类型的值',
  `status` int(255) NOT NULL DEFAULT '1' COMMENT '1 启用，2 禁用',
  `lock` int(1) NOT NULL DEFAULT '2' COMMENT '参数：1锁定，2开放',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='配置表';

-- ----------------------------
-- Records of gc_config
-- ----------------------------
INSERT INTO `gc_config` VALUES ('2', '0', 'cofnig', '基础配置', '', '0', '0', '1685597143', '1686793567', null, '1', '1');
INSERT INTO `gc_config` VALUES ('4', '2', 'title', '站点标题', 'Lg5.1轻量系统', '1', '0', '1685597143', '1692344301', null, '1', '1');
INSERT INTO `gc_config` VALUES ('8', '2', 'keywords', '关键字词', 'Tp5.1框架,Layui', '2', '0', '1685597143', '1687788088', null, '1', '1');
INSERT INTO `gc_config` VALUES ('9', '2', 'description', '站点描述', 'Lg5.1是一套开源免费的系统，采用自身轻量级模块化规范，遵循原生态的 Html/Css/JavaScript 开发模式。', '2', '0', '1685597143', '1687788088', null, '1', '1');

-- ----------------------------
-- Table structure for gc_files
-- ----------------------------
DROP TABLE IF EXISTS `gc_files`;
CREATE TABLE `gc_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(50) DEFAULT NULL COMMENT '文件名',
  `topic` varchar(255) DEFAULT NULL COMMENT '文件路径',
  `size` float(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '文件大小kb',
  `ext` varchar(10) DEFAULT NULL COMMENT '文件后缀',
  `Ip` varchar(50) DEFAULT NULL COMMENT '上传IP',
  `adminId` int(5) NOT NULL DEFAULT '0' COMMENT '管理员id',
  `lock` int(1) NOT NULL DEFAULT '2' COMMENT '参数：1锁定，2开放',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='附件表';

-- ----------------------------
-- Records of gc_files
-- ----------------------------
INSERT INTO `gc_files` VALUES ('27', '481a55642ab508e5c8d881c57798e482.jpg', 'uploads/20230818/481a55642ab508e5c8d881c57798e482.jpg', '143.53', 'jpg', '127.0.0.1', '1', '2', '1692345315');

-- ----------------------------
-- Table structure for gc_menu
-- ----------------------------
DROP TABLE IF EXISTS `gc_menu`;
CREATE TABLE `gc_menu` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` int(5) NOT NULL DEFAULT '0' COMMENT '上级菜单，0为顶级菜单',
  `title` varchar(50) DEFAULT NULL COMMENT '名称',
  `module` varchar(50) DEFAULT NULL COMMENT '模块',
  `controller` varchar(50) DEFAULT NULL COMMENT '控制器',
  `function` varchar(50) DEFAULT NULL COMMENT '方法',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '1显示在左侧菜单，2只作为节点',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT '1启用，2禁用',
  `orders` int(5) NOT NULL DEFAULT '0' COMMENT '排序值，越小越靠前',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '修改时间',
  `lock` int(1) NOT NULL DEFAULT '2' COMMENT '参数：1锁定，2开放',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='系统菜单表';

-- ----------------------------
-- Records of gc_menu
-- ----------------------------
INSERT INTO `gc_menu` VALUES ('2', '0', '系统管理', '', '', '', '1', '1', '0', 'layui-icon-app', '1685597143', '1686792723', '1');
INSERT INTO `gc_menu` VALUES ('3', '0', '参数设置', 'myadmin', 'config', 'myconfig', '1', '1', '0', 'layui-icon-set', '1685597143', '1685718964', '1');
INSERT INTO `gc_menu` VALUES ('4', '2', '参数列表', 'myadmin', 'config', 'index', '1', '1', '0', 'layui-icon-disabled', '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('5', '2', '菜单列表', 'myadmin', 'menu', 'index', '1', '1', '0', 'layui-icon-layouts', '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('9', '2', '管理列表', 'myadmin', 'admin', 'index', '1', '1', '0', 'layui-icon-user', '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('10', '2', '权限列表', 'myadmin', 'cate', 'index', '1', '1', '0', 'layui-icon-auz', '1685597143', '1685610560', '1');
INSERT INTO `gc_menu` VALUES ('11', '3', '查看', 'myadmin', 'config', 'myconfig', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('12', '3', '增改', 'myadmin', 'config', 'myedit', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('13', '4', '查看', 'myadmin', 'config', 'index', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('14', '4', '增改', 'myadmin', 'config', 'edit', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('15', '4', '删除', 'myadmin', 'config', 'del', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('16', '4', '状态', 'myadmin', 'config', 'status', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('17', '4', '排序', 'myadmin', 'config', 'orders', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('18', '5', '查看', 'myadmin', 'menu', 'index', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('19', '5', '增改', 'myadmin', 'menu', 'edit', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('20', '5', '删除', 'myadmin', 'menu', 'del', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('21', '5', '状态', 'myadmin', 'menu', 'status', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('22', '5', '排序', 'myadmin', 'menu', 'orders', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('23', '9', '查看', 'myadmin', 'admin', 'index', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('24', '9', '增改', 'myadmin', 'admin', 'edit', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('25', '9', '删除', 'myadmin', 'admin', 'del', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('26', '9', '状态', 'myadmin', 'admin', 'status', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('27', '10', '查看', 'myadmin', 'cate', 'index', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('28', '10', '增改', 'myadmin', 'cate', 'edit', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('29', '10', '删除', 'myadmin', 'cate', 'del', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('30', '10', '状态', 'myadmin', 'cate', 'status', '2', '1', '0', null, '1685597143', '1685597143', '1');
INSERT INTO `gc_menu` VALUES ('31', '0', '文章列表', 'myadmin', 'article', 'index', '1', '1', '0', 'layui-icon-read', '1685597143', '1686386534', '1');
INSERT INTO `gc_menu` VALUES ('32', '31', '查看', 'yadmin', 'article', 'index', '2', '1', '0', null, '1685597143', '1685597143', '2');
INSERT INTO `gc_menu` VALUES ('33', '31', '增改', 'myadmin', 'article', 'edit', '2', '1', '0', null, '1685597143', '1685597143', '2');
INSERT INTO `gc_menu` VALUES ('34', '31', '删除', 'myadmin', 'article', 'del', '2', '1', '0', null, '1685597143', '1685597143', '2');
INSERT INTO `gc_menu` VALUES ('35', '31', '状态', 'myadmin', 'article', 'status', '2', '1', '0', null, '1685597143', '1685597143', '2');
INSERT INTO `gc_menu` VALUES ('36', '31', '置顶', 'myadmin', 'article', 'top', '2', '1', '0', null, '1685597143', '1685597143', '2');
INSERT INTO `gc_menu` VALUES ('37', '0', '文件列表', 'myadmin', 'files', 'index', '1', '1', '0', 'layui-icon-picture', '1686386765', '1686386858', '2');
INSERT INTO `gc_menu` VALUES ('38', '37', '查看', 'myadmin', 'files', 'index', '2', '1', '0', null, '1686386765', '1686386765', '2');
INSERT INTO `gc_menu` VALUES ('39', '37', '删除', 'myadmin', 'files', 'del', '2', '1', '0', null, '1686386765', '1686386765', '2');
